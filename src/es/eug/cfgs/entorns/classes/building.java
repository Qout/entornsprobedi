/**
 * 
 */
package es.eug.cfgs.entorns.classes;

import es.eug.cfgs.entorns.classes.floor;
import es.eug.cfgs.entorns.classes.parking;

/**
 * The building class set the atributes for the floor classes 
 * and parking class and show them with a to String.
 * @author Alejandro
 *
 */
public class building {
	
	private floor fl1;
	private floor fl2;
	private floor fl3;
	private parking pk;
	/**
	 * Parametrized constructor
	 * 
	 * @param fl1
	 * @param fl2
	 * @param fl3
	 * @param pk
	 */
	public building(floor fl1, floor fl2, floor fl3, parking pk) {
		super();
		this.fl1 = fl1;
		this.fl2 = fl2;
		this.fl3 = fl3;
		this.pk = pk;
	}
	
	/**
	 * Parametrized constructor
	 * 
	 * @param fl1lp1nbm
	 * @param fl1lp1hh
	 * @param fl1lp1m2
	 * @param fl1lp2nbm
	 * @param fl1lp2hh
	 * @param fl1lp2m2
	 * @param fl1lp3nbm
	 * @param fl1lp3hh
	 * @param fl1lp3m2
	 * @param fl2lp1nbm
	 * @param fl2lp1hh
	 * @param fl2lp1m2
	 * @param fl2lp2nbm
	 * @param fl2lp2hh
	 * @param fl2lp2m2
	 * @param fl2lp3nbm
	 * @param fl2lp3hh
	 * @param fl2lp3m2
	 * @param fl3lp1nbm
	 * @param fl3lp1hh
	 * @param fl3lp1m2
	 * @param fl3lp2nbm
	 * @param fl3lp2hh
	 * @param fl3lp2m2
	 * @param fl3lp3nbm
	 * @param fl3lp3hh
	 * @param fl3lp3m2
	 * @param pknplaces
	 * @param m2
	 */
	
	public building(int fl1lp1nbm, boolean fl1lp1hh, double fl1lp1m2,
			int fl1lp2nbm, boolean fl1lp2hh, double fl1lp2m2,
			int fl1lp3nbm, boolean fl1lp3hh, double fl1lp3m2,
			int fl2lp1nbm, boolean fl2lp1hh, double fl2lp1m2,
			int fl2lp2nbm, boolean fl2lp2hh, double fl2lp2m2,
			int fl2lp3nbm, boolean fl2lp3hh, double fl2lp3m2,
			int fl3lp1nbm, boolean fl3lp1hh, double fl3lp1m2,
			int fl3lp2nbm, boolean fl3lp2hh, double fl3lp2m2,
			int fl3lp3nbm, boolean fl3lp3hh, double fl3lp3m2,
			int pknplaces, int m2){
		this.fl1 = new floor(fl1lp1nbm, fl1lp1hh, fl1lp1m2, fl1lp2nbm, fl1lp2hh, 
				fl1lp2m2, fl1lp3nbm, fl1lp3hh, fl1lp3m2);
		this.fl2 = new floor(fl2lp1nbm, fl2lp1hh, fl2lp1m2, fl2lp2nbm, fl2lp2hh, 
				fl2lp2m2, fl2lp3nbm, fl2lp3hh, fl2lp3m2);
		this.fl3 = new floor(fl3lp1nbm, fl3lp1hh, fl3lp1m2, fl3lp2nbm, fl3lp2hh, 
				fl3lp2m2, fl3lp3nbm, fl3lp3hh, fl3lp3m2);
		this.pk = new parking(pknplaces,m2);
	}
	/**
	 * Getter method for the Fl1 object
	 * 
	 * @return the fl1
	 */
	public floor getFl1() {
		return fl1;
	}
	/**
	 * Setter method for the Fl1 object
	 * 
	 * @param fl1 the fl1 to set
	 */
	public void setFl1(floor fl1) {
		this.fl1 = fl1;
	}
	/**
	 * Getter method for the Fl2 object
	 * 
	 * @return the fl2
	 */
	public floor getFl2() {
		return fl2;
	}
	/**
	 * Setter method for the Fl2 object
	 * 
	 * @param fl2 the fl2 to set
	 */
	public void setFl2(floor fl2) {
		this.fl2 = fl2;
	}
	/**
	 * Getter method for the Fl3 object
	 * 
	 * @return the fl3
	 */
	public floor getFl3() {
		return fl3;
	}
	/**
	 * Setter method for the Fl3 object
	 * 
	 * @param fl3 the fl3 to set
	 */
	public void setFl3(floor fl3) {
		this.fl3 = fl3;
	}
	/**
	 * Getter method for the pk attribute
	 * 
	 * @return the pk
	 */
	public parking getPk() {
		return pk;
	}
	/**
	 * Setter method for the pk attribute
	 * 
	 * @param pk the pk to set
	 */
	public void setPk(parking pk) {
		this.pk = pk;
	}
	
	/**
	 * toString method to show objects attributes
	 * 
	 * @void
	 */
	public String toString(){
		String ret = "** EDIFICI **\n"
				+this.fl1+this.fl2+this.fl3+this.pk;
		return ret;
	}

}
