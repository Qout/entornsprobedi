package es.eug.cfgs.entorns.classes;

/**
 * The livingpl class contains the information about each
 * living place of the floors.
 * 
 * @author Alejandro
 *
 */
public class livingpl {
	
	private int nbedroom;
	private boolean hasheating;
	private double m2;
	/**
	 * Parametrized constructor
	 * 
	 * @param nbedroom
	 * @param hasheating
	 * @param m2
	 */
	public livingpl(int nbedroom, boolean hasheating, double m2) {
		super();
		this.nbedroom = nbedroom;
		this.hasheating = hasheating;
		this.m2 = m2;
	}
	/**
	 * Getter method for the Nbedroom attribute
	 * 
	 * @return the nbedroom
	 */
	public int getNbedroom() {
		return nbedroom;
	}
	/**
	 * Setter method for the Nbedroom attribute
	 * 
	 * @param nbedroom the nbedroom to set
	 */
	public void setNbedroom(int nbedroom) {
		this.nbedroom = nbedroom;
	}
	/**
	 * Return the boolean content of attribute hasheating
	 * 
	 * @return the hasheating
	 */
	public boolean isHasheating() {
		return hasheating;
	}
	/**
	 * Setter method for the hasheating attribute
	 * 
	 * @param hasheating the hasheating to set
	 */
	public void setHasheating(boolean hasheating) {
		this.hasheating = hasheating;
	}
	/**
	 * Getter method for the M2 attribute
	 * 
	 * @return the m2
	 */
	public double getM2() {
		return m2;
	}
	/**
	 * Setter method for the M2 attribute
	 * 
	 * @param m2 the m2 to set
	 */
	public void setM2(double m2) {
		this.m2 = m2;
	}
	/**
	 * toString method to show objects attributes
	 * 
	 * @void
	 */
	public String toString(){
		String ret = "Livingplace:\n  -n� Rooms: "
				+this.nbedroom+"\n  -Heating? "
				+this.hasheating+"\n  -Dimensions: "
				+this.m2+"m2 \n";
		return ret;
	}

}
