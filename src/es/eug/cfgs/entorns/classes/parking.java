package es.eug.cfgs.entorns.classes;

/**
 * The parking class contains the information about
 * the parking.
 * 
 * @author Alejandro
 *
 */
public class parking {
	
	private int nplaces;
	private int m2;
	/**
	 * Parametrized constructor
	 * 
	 * @param nplaces
	 * @param m2
	 */
	public parking(int nplaces, int m2) {
		super();
		this.nplaces = nplaces;
		this.m2 = m2;
	}
	/**
	 * Getter method for the Nplaces attribute
	 * 
	 * @return the nplaces
	 */
	public int getNplaces() {
		return nplaces;
	}
	/**
	 * Setter method for the Nplaces attribute
	 * 
	 * @param nplaces the nplaces to set
	 */
	public void setNplaces(int nplaces) {
		this.nplaces = nplaces;
	}
	/**
	 * Returns the m2 attribute
	 * 
	 * @return the m2
	 */
	public int getM2() {
		return m2;
	}
	/**
	 * Setter method for the attribute m2
	 * 
	 * @param m2 the m2 to set
	 */
	public void setM2(int m2) {
		this.m2 = m2;
	}
	/**
	 * toString method to show objects attributes
	 * 
	 * @void
	 */
	public String toString(){
		String ret = "\nParking:\n  -n� Places: "
				+this.nplaces+"\n  -Dimensions: "
				+this.m2+"m2 \n";
		return ret;
	}

}
