/**
 * 
 */
package es.eug.cfgs.entorns.classes;

import es.eug.cfgs.entorns.classes.livingpl;

/**
 * The floor class organizes the information about
 * the living places in each floor
 * 
 * @author Alejandro
 *
 */
public class floor {
	
	private livingpl lp1;
	private livingpl lp2;
	private livingpl lp3;
	/**
	 * Parametrized constructor
	 * 
	 * @param lp1
	 * @param lp2
	 * @param lp3
	 */
	public floor(livingpl lp1, livingpl lp2, livingpl lp3) {
		super();
		this.lp1 = lp1;
		this.lp2 = lp2;
		this.lp3 = lp3;
	}
	/**
	 * Parametrized constructor
	 * 
	 * @param lp1nbm
	 * @param lp1hh
	 * @param lp1m2
	 * @param lp2nbm
	 * @param lp2hh
	 * @param lp2m2
	 * @param lp3nbm
	 * @param lp3hh
	 * @param lp3m2
	 */
	public floor(int lp1nbm, boolean lp1hh, double lp1m2,
			int lp2nbm, boolean lp2hh, double lp2m2,
			int lp3nbm, boolean lp3hh, double lp3m2){
		this.lp1 = new livingpl(lp1nbm, lp1hh, lp1m2);
		this.lp2 = new livingpl(lp2nbm, lp2hh, lp2m2);
		this.lp3 = new livingpl(lp3nbm, lp3hh, lp3m2);
	}
	/**
	 * Getter method for the attribute Lp1
	 * 
	 * @return the lp1
	 */
	public livingpl getLp1() {
		return lp1;
	}
	/**
	 * Setter method for the attribute Lp1
	 * 
	 * @param lp1 the lp1 to set
	 */
	public void setLp1(livingpl lp1) {
		this.lp1 = lp1;
	}
	/**
	 * Getter method for the attribute Lp2
	 * 
	 * @return the lp2
	 */
	public livingpl getLp2() {
		return lp2;
	}
	/**
	 * Setter method for the attribute Lp2
	 * 
	 * @param lp2 the lp2 to set
	 */
	public void setLp2(livingpl lp2) {
		this.lp2 = lp2;
	}
	/**
	 * Getter method for the attribute Lp3
	 * 
	 * @return the lp3
	 */
	public livingpl getLp3() {
		return lp3;
	}
	/**
	 * Setter method for the attribute Lp3
	 * 
	 * @param lp3 the lp3 to set
	 */
	public void setLp3(livingpl lp3) {
		this.lp3 = lp3;
	}
	/**
	 * toString method to show objects attributes
	 * 
	 * @void
	 */
	public String toString(){
		String ret ="\nFloor: \n"
				+"1."+this.lp1+"2."+this.lp2+"3."+this.lp3;
		return ret;
	}

}
