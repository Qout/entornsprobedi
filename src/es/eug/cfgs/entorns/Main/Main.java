package es.eug.cfgs.entorns.Main;

import es.eug.cfgs.entorns.classes.building;

/**
 * Main is just a Main class for the project, 
 * aiming to be a place to continue learning 
 * POO.
 * 
 * @author Alejandro Nicolas
 *
 */

public class Main {
	/**
	 * The execution will start with this method.
	 * 
	 * @param args Execution arguments
	 */
	public static void main(String[] args) {
		building b = new building(4,true,new Float(100.3),5,true,new Float(120.1),
				2,false,new Float(90.20),4,false,new Float(90.58),5,false,new Float(150.60),
				3,true,new Float(110.21),5,true,new Float(150.55),1,true,new Float(200.62),
				3,false,new Float(140.1),15,300);
		
		System.out.println(b);
	}

}
